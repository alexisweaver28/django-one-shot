from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list_list(request):
    todolist_list = TodoList.objects.all()
    context = {
        "todolist_list": todolist_list
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todolist_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todolist_detail": todolist_detail
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo_list_update = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list_update)
        if form.is_valid():
            todo_list_update = form.save()
            return redirect("todo_list_detail", id=todo_list_update.id)
    else:
        form = TodoListForm(instance=todo_list_update)
    context = {
        "form": form
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo_list_delete = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list_delete.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item_create = form.save()
            return redirect("todo_list_detail", id=todo_item_create.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form
    }
    return render(request, "todos/createitem.html", context)


def todo_item_update(request, id):
    todo_item_update = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item_update)
        if form.is_valid():
            todo_item_update = form.save()
            return redirect("todo_list_detail", id=todo_item_update.id)
    else:
        form = TodoItemForm(instance=todo_item_update)
    context = {
        "form": form
    }
    return render(request, "todos/edititem.html", context)
